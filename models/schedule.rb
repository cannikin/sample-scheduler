class Schedule < ActiveRecord::Base

  def next_time
    Rufus::Scheduler.parse(frequency).next_time
  end

end
