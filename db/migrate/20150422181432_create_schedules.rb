class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.string   :client
      t.text     :callback
      t.string   :frequency
      t.text     :last_response

      t.timestamps
    end
  end
end
