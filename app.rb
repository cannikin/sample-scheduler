RACK_ENV = (ENV['RACK_ENV'] || 'development').to_sym
require 'bundler'
Bundler.require(:default, RACK_ENV)
Dir["./jobs/*.rb"].each { |file| require_relative file }
Dir["./models/*.rb"].each { |file| require_relative file }

class Scheduler < Grape::API
  include Grape::ActiveRecord::Extension

  format :json
  default_format :json

  rescue_from ActiveRecord::RecordNotFound do |e|
    Rack::Response.new([ { :error => "Not found" }.to_json ], 404, { 'Content-Type' => 'application/json' }).finish
  end


  desc "Create a new schedule"
  params do
    requires :client,    :type => String
    requires :callback,  :type => String
    requires :frequency, :type => String
  end
  post "schedule" do
    schedule = Schedule.create params
    Delayed::Job.enqueue CallbackJob.new(schedule.id), :run_at => schedule.next_time
    schedule.as_json(:methods => :next_time)
  end


  desc "Get info about a schedule"
  params do
    requires :id, :type => Fixnum
  end
  get "schedule/:id" do
    Schedule.find(params[:id]).as_json(:methods => :next_time)
  end


  desc "Delete a schedule"
  params do
    requires :id, :type => Fixnum
  end
  delete "schedule/:id" do
    status 204
    Schedule.find(params[:id]).destroy
    body ''
  end

end
