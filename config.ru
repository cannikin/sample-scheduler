require './app'

# for activerecord pool management
use ActiveRecord::ConnectionAdapters::ConnectionManagement

run Rack::URLMap.new('/dj' => DelayedJobWeb.new,
                     '/api' => Scheduler.new)
