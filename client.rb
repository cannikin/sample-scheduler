require 'rack'
require 'rack/server'

class Client
  def self.call(env)
    [200, {}, ['']]
  end
end

Rack::Server.start :app => Client
