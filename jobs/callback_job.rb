class CallbackJob

  def initialize(schedule_id)
    @id = schedule_id
  end

  def perform
    @schedule = Schedule.find(@id)
    @response = Net::HTTP.post_form URI.parse(@schedule.callback), {}
  rescue ActiveRecord::RecordNotFound
    # schedule is no longer in the database, don't do anything
    @schedule = nil
  end

  def success(job)
    if @schedule
      puts "\nJob ran: #{@response.inspect}\n\n"
      @schedule.update :last_response => @response.inspect
      reschedule
    else
      # schedule is gone, don't reschedule
    end
  end

  def error(job, exception)
    # Anything special we want to do to record an error
  end

  private def reschedule
    Delayed::Job.enqueue CallbackJob.new(@schedule.id), :run_at => @schedule.next_time
  end

end
